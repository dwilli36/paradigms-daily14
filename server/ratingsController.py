import cherrypy
import re, json
from movies_library import _movie_database
class RatingsController(object):
    def __init__(self, mdb=None):
        if mdb is None:
            self.mdb = _movie_database()
            self.mdb.load_ratings('ratings.dat')
        else:
            self.mdb = mdb
    def GET_KEY(self, movie_id):
        '''when GET request for /movies/movie_id comes in, then we respond with json string'''
        output = {'result':'success'}
        movie_id = int(movie_id)
        try:
            rating = self.mdb.get_rating(movie_id)
            if rating is not None:
                output['rating'] = rating
                output['movie_id'] = movie_id
            else:
                output['result'] = 'error'
                output['message'] = 'movie not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
