console.log("tructus");

const submitButton = document.getElementById('send-button');
submitButton.onmouseup = getFormInfo;

const clearButton = document.getElementById('clear-button');
clearButton.onmouseup = clearResponse;

function getFormInfo(){
    // url 
    let url = document.getElementById('select-server-address').value;

    // port number
    let port = document.getElementById('input-port-number').value;

    // get the request type
    let radios = document.getElementsByName('bsr-radios');
    let type;
    for (let i = 0, length = radios.length; i < length; i++){
        if (radios[i].checked){
            type = radios[i].value;
            break;
        }
    }

    // get checkbox values for use key / use message body
    let key = false;
    let body = false;
    let key_val;
    let body_val;
    if (document.getElementById('checkbox-use-key').checked){
        key = true;
        key_val = document.getElementById('input-key').value;
    }
    if (document.getElementById('checkbox-use-message').checked){
        body = true;
        body_val = document.getElementById('text-message-body').value;
        console.log(typeof(body_val));
    }

    // perform appropriate request
    switch(type){
        case "GET":
            if (key){
                fetch(url + ":" + port + "/profile/" + key_val)
                    .then(response => response.json()).then( displayResponse );
            } else {
                fetch(url + ":" + port + "/movies/")
                    .then(response => response.json()).then( displayResponse );
            }
            break;
        case "PUT":
            if (body && key){
                fetch(url + ":" + port + "/profile/" + key_val, { method: 'PUT', mode: 'cors', body: body_val })
                    .then(response => response.json()).then( displayResponse );
            } else {
                displayResponse( {"error": "not a valid request"} )
            }
            break;
        case "POST":
            if (body){
                fetch(url + ":" + port + "/profile", { method: 'POST', mode: 'cors', body: body_val })
                    .then(response => response.json()).then( displayResponse );
            } else {
                displayResponse( {"error": "not a valid request"} )
            }
            break;
        case "DELETE":
            if (key){
                fetch(url + ":" + port + "/profile/" + key_val, { method: 'DELETE' })
                    .then(response => response.json()).then( displayResponse );
            } else {
                fetch(url + ":" + port + "/movies/", { method: 'DELETE' })
                    .then(response => response.json()).then( displayResponse );
            }
            break;
        default:
            console.log("No http request type selected");
    }
}

function displayResponse(response){
    let response_html = document.getElementById('answer-label');
    response_html.innerHTML = JSON.stringify(response);
}

function clearResponse(){
    let response_html = document.getElementById('answer-label');
    response_html.innerHTML = "";
}
